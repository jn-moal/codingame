package main

import (
	"fmt"
	"sort"
)

const (
	ME       = 1
	OPPONENT = -1
	NOBODY   = 0

	TYPE_FACTORY = "FACTORY"
	TYPE_TROOP   = "TROOP"

	MOVE = "MOVE"
	WAIT = "WAIT"
)

type factory struct {
	id, owner, nbCyborgs, production int
}

type link struct {
	fact1, fact2, distance int
	interest               float64
}

type byInterest []*link

func (b byInterest) Len() int           { return len(b) }
func (b byInterest) Swap(i, j int)      { b[i], b[j] = b[j], b[i] }
func (b byInterest) Less(i, j int) bool { return b[i].interest > b[j].interest }

type troop struct {
	factorySource, factoryDest, nbCyborgs, ETA int
}

type game struct {
	links     []*link
	factories map[int]*factory
}

func main() {
	var g game
	// factoryCount: the number of factories
	var factoryCount int
	fmt.Scan(&factoryCount)
	g.factories = make(map[int]*factory, 0)

	// linkCount: the number of links between factories
	var linkCount int
	fmt.Scan(&linkCount)
	g.links = make([]*link, linkCount)

	for i := 0; i < linkCount; i++ {
		var factory1, factory2, distance int
		fmt.Scan(&factory1, &factory2, &distance)
		g.links[i] = &link{fact1: factory1, fact2: factory2, distance: distance}
	}

	for {
		// entityCount: the number of entities (e.g. factories and troops)
		var entityCount int
		fmt.Scan(&entityCount)

		for i := 0; i < entityCount; i++ {
			var entityId int
			var entityType string
			var arg1, arg2, arg3, arg4, arg5 int
			fmt.Scan(&entityId, &entityType, &arg1, &arg2, &arg3, &arg4, &arg5)

			if entityType == TYPE_FACTORY {
				g.factories[entityId] = &factory{
					id:         entityId,
					owner:      arg1,
					nbCyborgs:  arg2,
					production: arg3,
				}
			}
		}

		action := g.actionByInterest()
		if action == "" {
			fmt.Println(WAIT)
		} else {
			fmt.Println(action)
		}
	}
}

// returns the factory owned by owner and present in link
func (g *game) getFactoryOf(owner int, l *link) *factory {
	if g.factories[l.fact1].owner == owner {
		return g.factories[l.fact1]
	}
	if g.factories[l.fact2].owner == owner {
		return g.factories[l.fact2]
	}

	return nil
}

// return a value that defines the interest of the link
// -1 can't do anything with this link
// 5 points if the link is takeable
// 10 points if it is owned by nobody
// from 0 to 60 points according the production of the factory
// plus the number of cyborg on my factory
// divide by the distance
func (g *game) interest(l *link) float64 {
	myFact := g.getFactoryOf(ME, l)
	otherFact := g.getFactoryOf(NOBODY, l)
	if otherFact == nil {
		otherFact = g.getFactoryOf(OPPONENT, l)
	}

	// If I don't own a factory, then I can't do anything with this link.
	if myFact == nil || otherFact == nil {
		return -1
	}

	nbCyborgs := computeNbCyborgs(otherFact, l)
	if myFact.nbCyborgs <= nbCyborgs {
		return -1
	}

	result := 5.
	if otherFact.owner == NOBODY {
		result += 10
	}
	result += 20. * float64(otherFact.production)
	result /= (float64(l.distance) + float64(nbCyborgs))

	return result
}

func computeNbCyborgs(other *factory, l *link) int {
	result := other.nbCyborgs + 2
	if other.owner == OPPONENT {
		result += (other.production * l.distance)
	}

	return result
}

// Game start, try to take all nobody productible factories
func (g *game) actionByInterest() (action string) {
	// Compute the interest of each link, and only act on the 3 most interesting one
	for _, link := range g.links {
		link.interest = g.interest(link)
	}

	for i := 0; i < 10; i++ {
		sort.Sort(byInterest(g.links)) //sort by interest
		for _, link := range g.links {
			if link.interest != -1 {
				myFact := g.getFactoryOf(ME, link)
				otherFact := g.getFactoryOf(NOBODY, link)
				if otherFact == nil {
					otherFact = g.getFactoryOf(OPPONENT, link)
				}

				if myFact != nil && otherFact != nil {
					if action != "" {
						action += ";"
					}
					nbCyborgs := computeNbCyborgs(otherFact, link)
					// Create an action to take this link
					action += fmt.Sprintf("%s %d %d %d", MOVE, myFact.id, otherFact.id, nbCyborgs)
					myFact.nbCyborgs -= nbCyborgs
					link.interest = g.interest(link)
				}
			}
		}
	}

	if action == "" {
		action = WAIT
	}
	return action
}
